﻿using System;

public enum MenuOption
{
    TestName,
    GuessNumber,
    Quit
}

public class Program
{
    private static MenuOption ReadUserOption()
    {
        int option;

        do
        {
            Console.WriteLine("Choose an option:");
            Console.WriteLine("1: Test Name");
            Console.WriteLine("2: Guess That Number");
            Console.WriteLine("3: Quit");

            Console.Write("[1-3]: ");
            option = Convert.ToInt32(Console.ReadLine());
        } while (option < 1 || option > 3);

        return (MenuOption)(option - 1);
    }

    private static void TestName()
    {
        Console.WriteLine("Enter your name: ");
        string name = Console.ReadLine();

        if (name.ToLower() == "gagana")
        {
            Console.WriteLine("Welcome my creator!");
        }
        else if (name.ToLower() == "wishwa")
        {
            Console.WriteLine("Welcome, friend!");
        }
        else if (name.ToLower() == "omalka")
        {
            Console.WriteLine("Welcome, friend!");
        }
        else
        {
            Console.WriteLine($"Hello {name}!");
        }
    }

    private static int ReadGuess(int min, int max)
    {
        int guess;

        do
        {
            Console.WriteLine($"Enter your guess between {min} and {max}: ");
            guess = Convert.ToInt32(Console.ReadLine());
        } while (guess < min || guess > max);

        return guess;
    }

    private static void RunGuessThatNumber()
    {
        Random random = new Random();
        int target = random.Next(1, 101);
        int min = 1;
        int max = 100;

        Console.WriteLine("Guess a number between 1 and 100:");

        int guess;
        do
        {
            guess = ReadGuess(min, max);

            if (guess < target)
            {
                Console.WriteLine("Your guess is too low.");
                min = guess + 1;
            }
            else if (guess > target)
            {
                Console.WriteLine("Your guess is too high.");
                max = guess - 1;
            }
            else
            {
                Console.WriteLine("Congratulations! You guessed the number!");
            }
        } while (guess != target);
    }

    public static void Main()
    {
        MenuOption user;

        do
        {
            user = ReadUserOption();

            switch (user)
            {
                case MenuOption.TestName:
                    TestName();
                    break;
                case MenuOption.GuessNumber:
                    RunGuessThatNumber();
                    break;
            }

        } while (user != MenuOption.Quit);
    }
}