﻿using System;

public class Program
{
    private static Bank _bank = new Bank();

    public enum MenuOption
    {
        NewAccount,
        Deposit,
        Withdraw,
        Transfer,
        Quit
    }

    public static void Main(string[] args)
    {
        while (true)
        {
            switch (ReadUserOption())
            {
                case MenuOption.NewAccount:
                    CreateNewAccount();
                    break;
                case MenuOption.Deposit:
                    DoDeposit();
                    break;
                case MenuOption.Withdraw:
                    DoWithdraw();
                    break;
                case MenuOption.Transfer:
                    DoTransfer();
                    break;
                case MenuOption.Quit:
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Invalid option. Please try again.");
                    break;
            }
        }
    }

    static MenuOption ReadUserOption()
    {
        Console.WriteLine("Choose an option:");
        Console.WriteLine("1. New Account");
        Console.WriteLine("2. Deposit");
        Console.WriteLine("3. Withdraw");
        Console.WriteLine("4. Transfer");
        Console.WriteLine("5. Quit");

        int option;
        if (int.TryParse(Console.ReadLine(), out option))
        {
            return (MenuOption)(option - 1);
        }
        else
        {
            return MenuOption.Quit;
        }
    }

    static void CreateNewAccount()
    {
        Console.Write("Enter account name: ");
        string name = Console.ReadLine();
        Console.Write("Enter starting balance: ");
        double balance = double.Parse(Console.ReadLine());

        Account newAccount = new Account(name, balance);
        _bank.AddAccount(newAccount);
        Console.WriteLine($"New account '{name}' created with balance {balance:C}");
    }

    static void DoDeposit()
    {
        Account account = FindAccount(_bank);
        if (account == null) return;

        Console.Write("Enter amount to deposit: ");
        double amount = double.Parse(Console.ReadLine());

        Bank.DepositTransaction transaction = new Bank.DepositTransaction(account, amount);
        _bank.ExecuteTransaction(transaction);
        transaction.Print();
    }

    static void DoWithdraw()
    {
        Account account = FindAccount(_bank);
        if (account == null) return;

        Console.Write("Enter amount to withdraw: ");
        double amount = double.Parse(Console.ReadLine());

        Bank.WithdrawTransaction transaction = new Bank.WithdrawTransaction(account, amount);
        _bank.ExecuteTransaction(transaction);
        transaction.Print();
    }

    static void DoTransfer()
    {
        Console.WriteLine("Transfer from:");
        Account fromAccount = FindAccount(_bank);
        if (fromAccount == null) return;

        Console.WriteLine("Transfer to:");
        Account toAccount = FindAccount(_bank);
        if (toAccount == null) return;

        Console.Write("Enter amount to transfer: ");
        double amount = double.Parse(Console.ReadLine());

        Bank.TransferTransaction transaction = new Bank.TransferTransaction(fromAccount, toAccount, amount);
        _bank.ExecuteTransaction(transaction);
        transaction.Print();
    }

    private static Account FindAccount(Bank bank)
    {
        Console.Write("Enter account name: ");
        string name = Console.ReadLine();
        Account result = bank.GetAccount(name);
        if (result == null)
        {
            Console.WriteLine($"No account found with name {name}");
        }
        return result;
    }
}
