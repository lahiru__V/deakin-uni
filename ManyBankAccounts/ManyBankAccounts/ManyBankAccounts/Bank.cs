﻿using System;
using System.Collections.Generic;

public class Bank
{
    private readonly List<Account> _accounts = new List<Account>();

    public void AddAccount(Account account)
    {
        _accounts.Add(account);
    }

    public Account GetAccount(string name)
    {
        foreach (var account in _accounts)
        {
            if (account.Name == name)
            {
                return account;
            }
        }
        return null;
    }

    public void ExecuteTransaction(Transaction transaction)
    {
        transaction.Execute();
    }

    public void ExecuteTransaction(DepositTransaction transaction)
    {
        transaction.Execute();
    }

    public void ExecuteTransaction(WithdrawTransaction transaction)
    {
        transaction.Execute();
    }

    public void ExecuteTransaction(TransferTransaction transaction)
    {
        transaction.Execute();
    }

    public abstract class Transaction
    {
        public Account Account { get; }
        public double Amount { get; }

        protected Transaction(Account account, double amount)
        {
            Account = account;
            Amount = amount;
        }

        public abstract void Execute();
        public abstract void Print();
    }

    public class DepositTransaction : Transaction
    {
        public DepositTransaction(Account account, double amount) : base(account, amount)
        {
        }

        public override void Execute()
        {
            Account.Deposit(Amount);
        }

        public override void Print()
        {
            Console.WriteLine($"Deposited {Amount:C} into account '{Account.Name}'. New balance: {Account.Balance:C}");
        }
    }

    public class WithdrawTransaction : Transaction
    {
        public WithdrawTransaction(Account account, double amount) : base(account, amount)
        {
        }

        public override void Execute()
        {
            if (!Account.Withdraw(Amount))
            {
                Console.WriteLine("Withdrawal failed.");
            }
        }

        public override void Print()
        {
            Console.WriteLine($"Withdrawn {Amount:C} from account '{Account.Name}'. New balance: {Account.Balance:C}");
        }
    }

    public class TransferTransaction : Transaction
    {
        private Account _toAccount;

        public TransferTransaction(Account fromAccount, Account toAccount, double amount) : base(fromAccount, amount)
        {
            _toAccount = toAccount;
        }

        public override void Execute()
        {
            if (Account.Withdraw(Amount))
            {
                _toAccount.Deposit(Amount);
                Console.WriteLine($"Transferred {Amount:C} from account '{Account.Name}' to '{_toAccount.Name}'.");
            }
            else
            {
                Console.WriteLine("Transfer failed.");
            }
        }

        public override void Print()
        {
            Console.WriteLine($"Transferred {Amount:C} from account '{Account.Name}' to '{_toAccount.Name}'.");
        }
    }
}
