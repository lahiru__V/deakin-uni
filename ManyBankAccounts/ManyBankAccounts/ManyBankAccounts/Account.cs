﻿using System;

public class Account
{
    public string Name { get; }
    public double Balance { get; private set; }

    public Account(string name, double balance)
    {
        Name = name;
        Balance = balance;
    }

    public void Deposit(double amount)
    {
        Balance += amount;
    }

    public bool Withdraw(double amount)
    {
        if (Balance >= amount)
        {
            Balance -= amount;
            return true;
        }
        else
        {
            Console.WriteLine("Insufficient funds.");
            return false;
        }
    }
}
