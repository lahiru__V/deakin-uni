using System;
using System.Drawing;
using System.Windows.Forms;

namespace RobotDodge
{
    class Program : Form
    {
        private Player player;

        public Program()
        {
            Text = "Robot Dodge";
            Size = new Size(800, 600);

            player = new Player();
            KeyDown += Program_KeyDown;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            player.Draw(e.Graphics);
        }

        private void Program_KeyDown(object sender, KeyEventArgs e)
        {
            player.HandleInput(e);
            player.StayOnWindow(ClientSize.Width, ClientSize.Height);
            Invalidate();

            if (player.Quit || e.KeyCode == Keys.Escape)
            {
                Close();
            }
        }

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Program program = new Program();
            program.Show();


            while (!program.player.Quit && program.Visible)
            {
                Application.DoEvents();
            }

            program.Dispose();
        }
    }
}
