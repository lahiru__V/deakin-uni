﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace RobotDodge
{
    public class Player
    {
        private const int SPEED = 5;
        private const int GAP = 10;

        private Bitmap _playerBitmap;

        public int X { get; private set; }
        public int Y { get; private set; }
        public int Width => _playerBitmap.Width;
        public int Height => _playerBitmap.Height;

        public bool Quit { get; private set; }

        public Player()
        {
            _playerBitmap = new Bitmap(@"E:\\newgame\images.jpg");
            X = 200;
            Y = 200;
            Quit = false;
        }

        public void Draw(Graphics g)
        {
            g.DrawImage(_playerBitmap, X, Y);
        }

        public void HandleInput(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                    Y -= SPEED;
                    break;
                case Keys.Down:
                    Y += SPEED;
                    break;
                case Keys.Left:
                    X -= SPEED;
                    break;
                case Keys.Right:
                    X += SPEED;
                    break;
                case Keys.Escape:
                    Quit = true;
                    break;
            }
        }

        public void StayOnWindow(int windowWidth, int windowHeight)
        {

            X = Math.Max(GAP, Math.Min(X, windowWidth - Width - GAP));
            Y = Math.Max(GAP, Math.Min(Y, windowHeight - Height - GAP));
        }
    }
}
