﻿using System;

namespace SimpleBank
{
    public class Account
    {
        public decimal Balance { get; private set; }

        public bool Deposit(decimal amountToDeposit)
        {
            if (amountToDeposit > 0)
            {
                Balance += amountToDeposit;
                return true;
            }
            return false;
        }

        public bool Withdraw(decimal amountToWithdraw)
        {
            if (amountToWithdraw > 0 && amountToWithdraw <= Balance)
            {
                Balance -= amountToWithdraw;
                return true;
            }
            return false;
        }

        public void Print()
        {
            Console.WriteLine($"Current Balance: {Balance:C}");
        }
    }
}
