﻿using System;
using System.Security.Principal;

namespace SimpleBank
{
    class Program
    {
        enum MenuOption
        {
            Withdraw = 1,
            Deposit,
            Print,
            Quit
        }

        static void Main(string[] args)
        {
            Account account = new Account();
            MenuOption option;

            do
            {
                option = ReadUserOption();
                switch (option)
                {
                    case MenuOption.Withdraw:
                        DoWithdraw(account);
                        break;
                    case MenuOption.Deposit:
                        DoDeposit(account);
                        break;
                    case MenuOption.Print:
                        account.Print();
                        break;
                }
            } while (option != MenuOption.Quit);
        }

        private static MenuOption ReadUserOption()
        {
            int choice;
            do
            {
                Console.WriteLine("Choose an option:");
                Console.WriteLine("1. Withdraw");
                Console.WriteLine("2. Deposit");
                Console.WriteLine("3. Print");
                Console.WriteLine("4. Quit");
                Console.Write("Enter your choice (1-4): ");
            } while (!int.TryParse(Console.ReadLine(), out choice) || choice < 1 || choice > 4);

            return (MenuOption)choice;
        }

        private static void DoWithdraw(Account account)
        {
            decimal amountToWithdraw;
            do
            {
                Console.Write("Enter amount to withdraw: ");
            } while (!decimal.TryParse(Console.ReadLine(), out amountToWithdraw) || amountToWithdraw <= 0);

            if (account.Withdraw(amountToWithdraw))
            {
                Console.WriteLine("Withdrawal successful.");
            }
            else
            {
                Console.WriteLine("Withdrawal failed. Insufficient funds.");
            }
        }

        private static void DoDeposit(Account account)
        {
            decimal amountToDeposit;
            do
            {
                Console.Write("Enter amount to deposit: ");
            } while (!decimal.TryParse(Console.ReadLine(), out amountToDeposit) || amountToDeposit <= 0);

            if (account.Deposit(amountToDeposit))
            {
                Console.WriteLine("Deposit successful.");
            }
            else
            {
                Console.WriteLine("Deposit failed. Invalid amount.");
            }
        }
    }
}
