﻿using System.Drawing;

namespace RobotDodge
{
    public class Player
    {
        private Bitmap _playerBitmap;
        private const int MoveSpeed = 5; // Adjust movement speed as needed

        public int X { get; private set; }
        public int Y { get; private set; }
        public int Width => _playerBitmap.Width;
        public int Height => _playerBitmap.Height;

        public Player()
        {
            _playerBitmap = new Bitmap(@"E:\\gagana 2\WinFormsApp1\asd.jpg");
            X = 0; // Default position, you can adjust as needed
            Y = 0; // Default position, you can adjust as needed
        }

        public void Draw(Graphics g)
        {
            g.DrawImage(_playerBitmap, X, Y);
        }

        public void MoveUp()
        {
            Y -= MoveSpeed;
        }

        public void MoveDown()
        {
            Y += MoveSpeed;
        }

        public void MoveLeft()
        {
            X -= MoveSpeed;
        }

        public void MoveRight()
        {
            X += MoveSpeed;
        }
    }
}
