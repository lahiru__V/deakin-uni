using System;
using System.Drawing;
using System.Windows.Forms;

namespace RobotDodge
{
    class Program : Form
    {
        private Player player;

        public Program()
        {
            Text = "Robot Dodge";
            Size = new Size(800, 600); // Adjust window size as needed

            player = new Player();
            KeyDown += Program_KeyDown;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            player.Draw(e.Graphics);
        }

        private void Program_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Up:
                    player.MoveUp();
                    break;
                case Keys.Down:
                    player.MoveDown();
                    break;
                case Keys.Left:
                    player.MoveLeft();
                    break;
                case Keys.Right:
                    player.MoveRight();
                    break;
            }
            Invalidate(); // Redraw the window after moving the player
        }

        [STAThread]
        static void Main()
        {
            Application.Run(new Program());
        }
    }
}
