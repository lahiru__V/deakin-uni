﻿using System;

public class DepositTransaction
{
    private Account _account;
    private decimal _amount;
    private bool _executed = false;
    private bool _success = false;
    private bool _reversed = false;

    public bool Success
    {
        get { return _success; }
    }

    public bool Reversed
    {
        get { return _reversed; }
    }

    public DepositTransaction(Account account, decimal amount)
    {
        _account = account;
        _amount = amount;
    }

    public void Execute()
    {
        if (_executed)
        {
            throw new Exception("Cannot execute this transaction as it has already been executed.");
        }

        _executed = true;
        _account.Deposit(_amount);
        _success = true;
    }

    public void Rollback()
    {
        throw new Exception("Deposit transaction cannot be rolled back.");
    }

    public void Print()
    {
        if (_executed)
        {
            if (_success)
            {
                Console.WriteLine($"Deposit of {_amount:C} into account {_account.AccountNumber} was successful.");
            }
            else
            {
                Console.WriteLine($"Deposit of {_amount:C} into account {_account.AccountNumber} failed.");
            }
        }
        else
        {
            Console.WriteLine("Transaction has not been executed yet.");
        }
    }
}
