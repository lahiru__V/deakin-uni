﻿using System;

public class TransferTransaction
{
    private Account _sourceAccount;
    private Account _destinationAccount;
    private decimal _amount;
    private bool _executed = false;
    private bool _success = false;
    private bool _reversed = false;

    public bool Success
    {
        get { return _success; }
    }

    public bool Reversed
    {
        get { return _reversed; }
    }

    public TransferTransaction(Account sourceAccount, Account destinationAccount, decimal amount)
    {
        _sourceAccount = sourceAccount;
        _destinationAccount = destinationAccount;
        _amount = amount;
    }

    public void Execute()
    {
        if (_executed)
        {
            throw new Exception("Cannot execute this transaction as it has already been executed.");
        }

        _executed = true;

        if (_sourceAccount.Withdraw(_amount))
        {
            _destinationAccount.Deposit(_amount);
            _success = true;
        }
    }

    public void Rollback()
    {
        if (!_executed)
        {
            throw new Exception("Cannot rollback this transaction as it has not been executed.");
        }

        if (_reversed)
        {
            throw new Exception("Cannot rollback this transaction as it has already been reversed.");
        }

        _reversed = true;

        // Reversing actions from execute
        _destinationAccount.Withdraw(_amount);
        _sourceAccount.Deposit(_amount);
    }

    public void Print()
    {
        if (_executed)
        {
            if (_success)
            {
                Console.WriteLine($"Transfer of {_amount:C} from account {_sourceAccount.AccountNumber} to account {_destinationAccount.AccountNumber} was successful.");
            }
            else
            {
                Console.WriteLine($"Transfer of {_amount:C} from account {_sourceAccount.AccountNumber} to account {_destinationAccount.AccountNumber} failed.");
            }
        }
        else
        {
            Console.WriteLine("Transaction has not been executed yet.");
        }

        if (_reversed)
        {
            Console.WriteLine("Transaction has been reversed.");
        }
    }
}
