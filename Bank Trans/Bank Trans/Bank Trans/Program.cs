﻿using System;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        List<Account> accounts = new List<Account>();

        while (true)
        {
            Console.WriteLine("1. Add Account");
            Console.WriteLine("2. Withdraw");
            Console.WriteLine("3. Deposit");
            Console.WriteLine("4. Transfer");
            Console.WriteLine("5. View Account Balance");
            Console.WriteLine("6. Exit");

            Console.Write("Enter your choice: ");
            int choice = int.Parse(Console.ReadLine());

            switch (choice)
            {
                case 1:
                    AddAccount(accounts);
                    break;
                case 2:
                    DoWithdraw(accounts);
                    break;
                case 3:
                    DoDeposit(accounts);
                    break;
                case 4:
                    DoTransfer(accounts);
                    break;
                case 5:
                    ViewAccountBalance(accounts);
                    break;
                case 6:
                    Console.WriteLine("Exiting program...");
                    return;
                default:
                    Console.WriteLine("Invalid choice!");
                    break;
            }
        }
    }

    static void AddAccount(List<Account> accounts)
    {
        Console.Write("Enter account number: ");
        string accNumber = Console.ReadLine();
        Console.Write("Enter initial balance: ");
        decimal initialBalance = decimal.Parse(Console.ReadLine());

        Account account = new Account(accNumber, initialBalance);
        accounts.Add(account);

        Console.WriteLine("Account added successfully!");
    }

    static void ViewAccountBalance(List<Account> accounts)
    {
        Console.Write("Enter account number: ");
        string accNumber = Console.ReadLine();

        Account account = accounts.Find(acc => acc.AccountNumber == accNumber);
        if (account != null)
        {
            Console.WriteLine($"Account balance for {account.AccountNumber}: {account.Balance:C}");
        }
        else
        {
            Console.WriteLine("Account not found!");
        }
    }

    static void DoWithdraw(List<Account> accounts)
    {
        Console.Write("Enter account number: ");
        string accNumber = Console.ReadLine();
        Console.Write("Enter withdrawal amount: ");
        decimal amount = decimal.Parse(Console.ReadLine());

        Account account = accounts.Find(acc => acc.AccountNumber == accNumber);
        if (account != null)
        {
            WithdrawTransaction withdraw = new WithdrawTransaction(account, amount);
            withdraw.Execute();
            withdraw.Print();
        }
        else
        {
            Console.WriteLine("Account not found!");
        }
    }

    static void DoDeposit(List<Account> accounts)
    {
        Console.Write("Enter account number: ");
        string accNumber = Console.ReadLine();
        Console.Write("Enter deposit amount: ");
        decimal amount = decimal.Parse(Console.ReadLine());

        Account account = accounts.Find(acc => acc.AccountNumber == accNumber);
        if (account != null)
        {
            DepositTransaction deposit = new DepositTransaction(account, amount);
            deposit.Execute();
            deposit.Print();
        }
        else
        {
            Console.WriteLine("Account not found!");
        }
    }

    static void DoTransfer(List<Account> accounts)
    {
        Console.Write("Enter source account number: ");
        string sourceAccNumber = Console.ReadLine();
        Console.Write("Enter destination account number: ");
        string destAccNumber = Console.ReadLine();
        Console.Write("Enter transfer amount: ");
        decimal amount = decimal.Parse(Console.ReadLine());

        Account sourceAccount = accounts.Find(acc => acc.AccountNumber == sourceAccNumber);
        Account destAccount = accounts.Find(acc => acc.AccountNumber == destAccNumber);

        if (sourceAccount != null && destAccount != null)
        {
            TransferTransaction transfer = new TransferTransaction(sourceAccount, destAccount, amount);
            transfer.Execute();
            transfer.Print();
        }
        else
        {
            Console.WriteLine("One or both accounts not found!");
        }
    }
}
