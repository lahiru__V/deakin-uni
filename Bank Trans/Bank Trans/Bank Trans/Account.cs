﻿using System;

public class Account
{
    public string AccountNumber { get; }
    public decimal Balance { get; private set; }

    public Account(string accountNumber, decimal initialBalance)
    {
        AccountNumber = accountNumber;
        Balance = initialBalance;
    }

    public bool Withdraw(decimal amount)
    {
        if (Balance >= amount)
        {
            Balance -= amount;
            return true;
        }
        return false;
    }

    public void Deposit(decimal amount)
    {
        Balance += amount;
    }
}
