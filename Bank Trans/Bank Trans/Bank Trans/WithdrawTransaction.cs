﻿using System;

public class WithdrawTransaction
{
    private Account _account;
    private decimal _amount;
    private bool _executed = false;
    private bool _success = false;
    private bool _reversed = false;

    public bool Success
    {
        get { return _success; }
    }

    public bool Reversed
    {
        get { return _reversed; }
    }

    public WithdrawTransaction(Account account, decimal amount)
    {
        _account = account;
        _amount = amount;
    }

    public void Execute()
    {
        if (_executed)
        {
            throw new Exception("Cannot execute this transaction as it has already been executed.");
        }

        _executed = true;
        _success = _account.Withdraw(_amount);
    }

    public void Rollback()
    {
        if (!_executed)
        {
            throw new Exception("Cannot rollback this transaction as it has not been executed.");
        }

        if (_reversed)
        {
            throw new Exception("Cannot rollback this transaction as it has already been reversed.");
        }

        _reversed = true;

        // Reversing actions from execute
        _account.Deposit(_amount);
    }

    public void Print()
    {
        if (_executed)
        {
            if (_success)
            {
                Console.WriteLine($"Withdrawal of {_amount:C} from account {_account.AccountNumber} was successful.");
            }
            else
            {
                Console.WriteLine($"Withdrawal of {_amount:C} from account {_account.AccountNumber} failed.");
            }
        }
        else
        {
            Console.WriteLine("Transaction has not been executed yet.");
        }

        if (_reversed)
        {
            Console.WriteLine("Transaction has been reversed.");
        }
    }
}
