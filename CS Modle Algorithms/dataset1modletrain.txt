import os
from collections import defaultdict
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import confusion_matrix, zero_one_loss, precision_score, recall_score, accuracy_score, f1_score
from sklearn.preprocessing import StandardScaler
import warnings
warnings.filterwarnings('ignore')
import requests

# Downloading the data
link_to_data = 'https://raw.githubusercontent.com/SIT719/2020-S2/master/data/Week_5_NSL-KDD-Dataset/training_attack_types.txt?raw=true'
DataSet = "training_attack_types.txt"
if not os.path.exists(DataSet):
    with open(DataSet, "wb") as f:
        response = requests.get(link_to_data)
        f.write(response.content)

# Defining column names and indices
header_names = ['duration', 'protocol_type', 'service', 'flag', 'src_bytes', 'dst_bytes', 'land', 'wrong_fragment', 'urgent', 'hot', 'num_failed_logins', 'logged_in', 'num_compromised', 'root_shell', 'su_attempted', 'num_root', 'num_file_creations', 'num_shells', 'num_access_files', 'num_outbound_cmds', 'is_host_login', 'is_guest_login', 'count', 'srv_count', 'serror_rate', 'srv_serror_rate', 'rerror_rate', 'srv_rerror_rate', 'same_srv_rate', 'diff_srv_rate', 'srv_diff_host_rate', 'dst_host_count', 'dst_host_srv_count', 'dst_host_same_srv_rate', 'dst_host_diff_srv_rate', 'dst_host_same_src_port_rate', 'dst_host_srv_diff_host_rate', 'dst_host_serror_rate', 'dst_host_srv_serror_rate', 'dst_host_rerror_rate', 'dst_host_srv_rerror_rate', 'attack_type', 'success_pred']
col_names = np.array(header_names)

nominal_idx = [1, 2, 3]
binary_idx = [6, 11, 13, 14, 20, 21]
numeric_idx = list(set(range(41)).difference(nominal_idx).difference(binary_idx))

nominal_cols = col_names[nominal_idx].tolist()
binary_cols = col_names[binary_idx].tolist()
numeric_cols = col_names[numeric_idx].tolist()

# Creating category mappings
category = defaultdict(list)
category['benign'].append('normal')

with open(DataSet, 'r') as f:
    for line in f.readlines():
        attack, cat = line.strip().split(' ')
        category[cat].append(attack)

attack_mapping = dict((v,k) for k in category for v in category[k])

# Loading train and test data
train_file='https://raw.githubusercontent.com/SIT719/2020-S2/master/data/Week_5_NSL-KDD-Dataset/KDDTrain%2B.txt'
train_df = pd.read_csv(train_file, names=header_names)
train_df['attack_category'] = train_df['attack_type'].map(lambda x: attack_mapping[x])
train_df.drop(['success_pred'], axis=1, inplace=True)

test_file='https://raw.githubusercontent.com/SIT719/2020-S2/master/data/Week_5_NSL-KDD-Dataset/KDDTest%2B.txt'
test_df = pd.read_csv(test_file, names=header_names)
test_df['attack_category'] = test_df['attack_type'].map(lambda x: attack_mapping[x])
test_df.drop(['success_pred'], axis=1, inplace=True)

# Dropping unnecessary features
train_df.drop('num_outbound_cmds', axis = 1, inplace=True)
test_df.drop('num_outbound_cmds', axis = 1, inplace=True)
numeric_cols.remove('num_outbound_cmds')

# Data Preparation
train_Y = train_df['attack_category']
train_x_raw = train_df.drop(['attack_category','attack_type'], axis=1)
test_Y = test_df['attack_category']
test_x_raw = test_df.drop(['attack_category','attack_type'], axis=1)

combined_df_raw = pd.concat([train_x_raw, test_x_raw])
combined_df = pd.get_dummies(combined_df_raw, columns=nominal_cols, drop_first=True)

train_x = combined_df[:len(train_x_raw)]
test_x = combined_df[len(train_x_raw):]

# Scaling numeric features using StandardScaler
standard_scaler = StandardScaler().fit(train_x[numeric_cols])

train_x[numeric_cols] = standard_scaler.transform(train_x[numeric_cols])
test_x[numeric_cols] = standard_scaler.transform(test_x[numeric_cols])

train_Y_bin = train_Y.apply(lambda x: 0 if x == 'benign' else 1)
test_Y_bin = test_Y.apply(lambda x: 0 if x == 'benign' else 1)

# Decision Tree Classifier
dt_classifier = DecisionTreeClassifier(random_state=17)
dt_classifier.fit(train_x, train_Y)

dt_pred_y = dt_classifier.predict(test_x)

dt_results = confusion_matrix(test_Y, dt_pred_y)
dt_error = zero_one_loss(test_Y, dt_pred_y)
dt_accuracy = accuracy_score(test_Y, dt_pred_y)
dt_f1 = f1_score(test_Y, dt_pred_y, average='weighted')
dt_precision = precision_score(test_Y, dt_pred_y, average='weighted')
dt_recall = recall_score(test_Y, dt_pred_y, average='weighted')

print("Decision Tree Classifier Results:")
print("Confusion Matrix:")
print(dt_results)
print("Error Rate:", dt_error)
print("Accuracy:", dt_accuracy)
print("F1 Score:", dt_f1)
print("Precision:", dt_precision)
print("Recall:", dt_recall)

# K Nearest Neighbors Classifier
knn_classifier = KNeighborsClassifier()
knn_classifier.fit(train_x, train_Y)

knn_pred_y = knn_classifier.predict(test_x)

knn_results = confusion_matrix(test_Y, knn_pred_y)
knn_error = zero_one_loss(test_Y, knn_pred_y)
knn_accuracy = accuracy_score(test_Y, knn_pred_y)
knn_f1 = f1_score(test_Y, knn_pred_y, average='weighted')
knn_precision = precision_score(test_Y, knn_pred_y, average='weighted')
knn_recall = recall_score(test_Y, knn_pred_y, average='weighted')

print("\nK Nearest Neighbors Classifier Results:")
print("Confusion Matrix:")
print(knn_results)
print("Error Rate:", knn_error)
print("Accuracy:", knn_accuracy)
print("F1 Score:", knn_f1)
print("Precision:", knn_precision)
print("Recall:", knn_recall)

# Logistic Regression Classifier
lr_classifier = LogisticRegression(max_iter=1000)
lr_classifier.fit(train_x, train_Y)

lr_pred_y = lr_classifier.predict(test_x)

lr_results = confusion_matrix(test_Y, lr_pred_y)
lr_error = zero_one_loss(test_Y, lr_pred_y)
lr_accuracy = accuracy_score(test_Y, lr_pred_y)
lr_f1 = f1_score(test_Y, lr_pred_y, average='weighted')
lr_precision = precision_score(test_Y, lr_pred_y, average='weighted')
lr_recall = recall_score(test_Y, lr_pred_y, average='weighted')

print("\nLogistic Regression Classifier Results:")
print("Confusion Matrix:")
print(lr_results)
print("Error Rate:", lr_error)
print("Accuracy:", lr_accuracy)
print("F1 Score:", lr_f1)
print("Precision:", lr_precision)
print("Recall:", lr_recall)

# Naive Bayes Classifier
nb_classifier = GaussianNB()
nb_classifier.fit(train_x, train_Y)

nb_pred_y = nb_classifier.predict(test_x)

nb_results = confusion_matrix(test_Y, nb_pred_y)
nb_error = zero_one_loss(test_Y, nb_pred_y)
nb_accuracy = accuracy_score(test_Y, nb_pred_y)
nb_f1 = f1_score(test_Y, nb_pred_y, average='weighted')
nb_precision = precision_score(test_Y, nb_pred_y, average='weighted')
nb_recall = recall_score(test_Y, nb_pred_y, average='weighted')

print("\nNaive Bayes Classifier Results:")
print("Confusion Matrix:")
print(nb_results)
print("Error Rate:", nb_error)
print("Accuracy:", nb_accuracy)
print("F1 Score:", nb_f1)
print("Precision:", nb_precision)
print("Recall:", nb_recall)

# Gradient Boosting Classifier
grb_classifier = GradientBoostingClassifier(random_state=17)
grb_classifier.fit(train_x, train_Y)

grb_pred_y = grb_classifier.predict(test_x)

grb_results = confusion_matrix(test_Y, grb_pred_y)
grb_error = zero_one_loss(test_Y, grb_pred_y)
grb_accuracy = accuracy_score(test_Y, grb_pred_y)
grb_f1 = f1_score(test_Y, grb_pred_y, average='weighted')
grb_precision = precision_score(test_Y, grb_pred_y, average='weighted')
grb_recall = recall_score(test_Y, grb_pred_y, average='weighted')

print("\nGradient Boosting Classifier Results:")
print("Confusion Matrix:")
print(grb_results)
print("Error Rate:", grb_error)
print("Accuracy:", grb_accuracy)
print("F1 Score:", grb_f1)
print("Precision:", grb_precision)
print("Recall:", grb_recall)

attack_categories = ['dos', 'normal', 'probe', 'r2l', 'u2r']

# Decision Tree Classifier
precision_scores_dt = {}
recall_scores_dt = {}
f1_scores_dt = {}

for category in attack_categories:
    test_Y_category = (test_Y == category)
    pred_Y_category = (dt_pred_y == category)
    
    precision = precision_score(test_Y_category, pred_Y_category)
    recall = recall_score(test_Y_category, pred_Y_category)
    f1 = f1_score(test_Y_category, pred_Y_category)
    
    precision_scores_dt[category] = precision
    recall_scores_dt[category] = recall
    f1_scores_dt[category] = f1

print("\nDecision Tree Classifier Scores:")
print("Precision Scores:")
for category, precision in precision_scores_dt.items():
    print(f"{category}: {precision:.4f}")

print("\nRecall Scores:")
for category, recall in recall_scores_dt.items():
    print(f"{category}: {recall:.4f}")

print("\nF1 Scores:")
for category, f1 in f1_scores_dt.items():
    print(f"{category}: {f1:.4f}")

weighted_precision_dt = sum(precision_scores_dt.values()) / len(precision_scores_dt)
weighted_recall_dt = sum(recall_scores_dt.values()) / len(recall_scores_dt)
weighted_f1_dt = sum(f1_scores_dt.values()) / len(f1_scores_dt)

print("Overall Precision (nPrecision):", weighted_precision_dt)
print("Overall Recall (nRecall):", weighted_recall_dt)
print("Overall F1 Score:", weighted_f1_dt)

# K Nearest Neighbors Classifier
precision_scores_knn = {}
recall_scores_knn = {}
f1_scores_knn = {}

for category in attack_categories:
    test_Y_category = (test_Y == category)
    pred_Y_category = (knn_pred_y == category)
    
    precision = precision_score(test_Y_category, pred_Y_category)
    recall = recall_score(test_Y_category, pred_Y_category)
    f1 = f1_score(test_Y_category, pred_Y_category)
    
    precision_scores_knn[category] = precision
    recall_scores_knn[category] = recall
    f1_scores_knn[category] = f1

print("\nK Nearest Neighbors Classifier Scores:")
print("Precision Scores:")
for category, precision in precision_scores_knn.items():
    print(f"{category}: {precision:.4f}")

print("\nRecall Scores:")
for category, recall in recall_scores_knn.items():
    print(f"{category}: {recall:.4f}")

print("\nF1 Scores:")
for category, f1 in f1_scores_knn.items():
    print(f"{category}: {f1:.4f}")

weighted_precision_knn = sum(precision_scores_knn.values()) / len(precision_scores_knn)
weighted_recall_knn = sum(recall_scores_knn.values()) / len(recall_scores_knn)
weighted_f1_knn = sum(f1_scores_knn.values()) / len(f1_scores_knn)

print("Overall Precision (nPrecision):", weighted_precision_knn)
print("Overall Recall (nRecall):", weighted_recall_knn)
print("Overall F1 Score:", weighted_f1_knn)

# Logistic Regression Classifier
precision_scores_lr = {}
recall_scores_lr = {}
f1_scores_lr = {}

for category in attack_categories:
    test_Y_category = (test_Y == category)
    pred_Y_category = (lr_pred_y == category)
    
    precision = precision_score(test_Y_category, pred_Y_category)
    recall = recall_score(test_Y_category, pred_Y_category)
    f1 = f1_score(test_Y_category, pred_Y_category)
    
    precision_scores_lr[category] = precision
    recall_scores_lr[category] = recall
    f1_scores_lr[category] = f1

print("\nLogistic Regression Classifier Scores:")
print("Precision Scores:")
for category, precision in precision_scores_lr.items():
    print(f"{category}: {precision:.4f}")

print("\nRecall Scores:")
for category, recall in recall_scores_lr.items():
    print(f"{category}: {recall:.4f}")

print("\nF1 Scores:")
for category, f1 in f1_scores_lr.items():
    print(f"{category}: {f1:.4f}")

weighted_precision_lr = sum(precision_scores_lr.values()) / len(precision_scores_lr)
weighted_recall_lr = sum(recall_scores_lr.values()) / len(recall_scores_lr)
weighted_f1_lr = sum(f1_scores_lr.values()) / len(f1_scores_lr)

print("Overall Precision (nPrecision):", weighted_precision_lr)
print("Overall Recall (nRecall):", weighted_recall_lr)
print("Overall F1 Score:", weighted_f1_lr)

# Naive Bayes Classifier
precision_scores_nb = {}
recall_scores_nb = {}
f1_scores_nb = {}

for category in attack_categories:
    test_Y_category = (test_Y == category)
    pred_Y_category = (nb_pred_y == category)
    
    precision = precision_score(test_Y_category, pred_Y_category)
    recall = recall_score(test_Y_category, pred_Y_category)
    f1 = f1_score(test_Y_category, pred_Y_category)
    
    precision_scores_nb[category] = precision
    recall_scores_nb[category] = recall
    f1_scores_nb[category] = f1

print("\nNaive Bayes Classifier Scores:")
print("Precision Scores:")
for category, precision in precision_scores_nb.items():
    print(f"{category}: {precision:.4f}")

print("\nRecall Scores:")
for category, recall in recall_scores_nb.items():
    print(f"{category}: {recall:.4f}")

print("\nF1 Scores:")
for category, f1 in f1_scores_nb.items():
    print(f"{category}: {f1:.4f}")

weighted_precision_nb = sum(precision_scores_nb.values()) / len(precision_scores_nb)
weighted_recall_nb = sum(recall_scores_nb.values()) / len(recall_scores_nb)
weighted_f1_nb = sum(f1_scores_nb.values()) / len(f1_scores_nb)

print("Overall Precision (nPrecision):", weighted_precision_nb)
print("Overall Recall (nRecall):", weighted_recall_nb)
print("Overall F1 Score:", weighted_f1_nb)

# Gradient Boosting Classifier
precision_scores_grb = {}
recall_scores_grb = {}
f1_scores_grb = {}

for category in attack_categories:
    test_Y_category = (test_Y == category)
    pred_Y_category = (grb_pred_y == category)
    
    precision = precision_score(test_Y_category, pred_Y_category)
    recall = recall_score(test_Y_category, pred_Y_category)
    f1 = f1_score(test_Y_category, pred_Y_category)
    
    precision_scores_grb[category] = precision
    recall_scores_grb[category] = recall
    f1_scores_grb[category] = f1

print("\nGradient Boosting Classifier Scores:")
print("Precision Scores:")
for category, precision in precision_scores_grb.items():
    print(f"{category}: {precision:.4f}")

print("\nRecall Scores:")
for category, recall in recall_scores_grb.items():
    print(f"{category}: {recall:.4f}")

print("\nF1 Scores:")
for category, f1 in f1_scores_grb.items():
    print(f"{category}: {f1:.4f}")

weighted_precision_grb = sum(precision_scores_grb.values()) / len(precision_scores_grb)
weighted_recall_grb = sum(recall_scores_grb.values()) / len(recall_scores_grb)
weighted_f1_grb = sum(f1_scores_grb.values()) / len(f1_scores_grb)

print("Overall Precision (nPrecision):", weighted_precision_grb)
print("Overall Recall (nRecall):", weighted_recall_grb)
print("Overall F1 Score:", weighted_f1_grb)
